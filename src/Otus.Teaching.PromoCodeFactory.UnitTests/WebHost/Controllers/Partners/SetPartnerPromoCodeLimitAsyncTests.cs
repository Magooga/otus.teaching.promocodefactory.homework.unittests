﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;

        //TODO: Add Unit Tests
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            //var mock = new Mock<IRepository<Partner>>();
            //_partnersController = new PartnersController(mock.Object);

            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IF_Partner_NotFound_Return_Eror404()
        {
            //Arrange
            //var mock = new Mock<IRepository<Partner>>();
            //PartnersController _partnersController = new PartnersController(mock.Object);

            Guid id = Guid.NewGuid();
            //SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest 
            //{ 
            //    EndDate = DateTime.Now, 
            //    Limit = 1 
            //};

            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(id)).ReturnsAsync(partner);

            var entitySetPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            //Act
            var actionResult = await _partnersController.SetPartnerPromoCodeLimitAsync(id, entitySetPartnerPromoCodeLimitRequest);
            //var statusCodeResult = (IStatusCodeActionResult)actionResult;

            //Assert
            actionResult.Should().BeAssignableTo<NotFoundResult>();
            //Assert.Equal(expectedCode, statusCodeResult.StatusCode);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IF_Partner_Is_Blocked_Return_Error400()
        {
            //Arrange
            Guid id = Guid.NewGuid();
            var entityPartner = new Fixture().Build<Partner>().With(p => p.IsActive, false).Without(e => e.PartnerLimits).Create();


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(id)).ReturnsAsync(entityPartner);

            var entitySetPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            //Act
            var actionResult = await _partnersController.SetPartnerPromoCodeLimitAsync(id, entitySetPartnerPromoCodeLimitRequest);
            

            //Assert
            actionResult.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_add_new_Limit_set_NumberIssuedPromoCodes_to_zero_and_set_CancelDate_to_old_Limit()
        {
            //Arrange
            Guid id = Guid.NewGuid();
            var entityPartner = new Fixture().Build<Partner>().With(p => p.IsActive, true).With(p => p.NumberIssuedPromoCodes, 10).With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>
            { 
                new PartnerPromoCodeLimit 
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2023, 03, 3),
                        EndDate = new DateTime(2023, 07, 7),
                        Limit = 99,
                        CancelDate = null
                }
            }).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(id)).ReturnsAsync(entityPartner);

            var entitySetPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            //Act
            var actionResult = await _partnersController.SetPartnerPromoCodeLimitAsync(id, entitySetPartnerPromoCodeLimitRequest);

            //Assert
            entityPartner.NumberIssuedPromoCodes.Should().Be(0);
            entityPartner.PartnerLimits.FirstOrDefault().CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IF_Limit_not_active_NumberIssuedPromoCodes_not_change()
        {
            //Arrange
            Guid id = Guid.NewGuid();
            var entityPartner = new Fixture().Build<Partner>().With(p => p.IsActive, true).With(p => p.NumberIssuedPromoCodes, 10).With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2023, 03, 3),
                        EndDate = new DateTime(2023, 07, 7),
                        Limit = 99,
                        CancelDate = new DateTime(2023, 07, 7)
                }
            }).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(id)).ReturnsAsync(entityPartner);

            var entitySetPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            //Act
            var actionResult = await _partnersController.SetPartnerPromoCodeLimitAsync(id, entitySetPartnerPromoCodeLimitRequest);

            //Assert
            entityPartner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_IF_request_Limit_equals_or_less_zero_Return_BadRequest(int limit)
        {
            //Arrange
            Guid id = Guid.NewGuid();

            var entityPartner = new Fixture().Build<Partner>().With(p => p.IsActive, true).With(p => p.NumberIssuedPromoCodes, 10).With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2023, 03, 3),
                        EndDate = new DateTime(2023, 07, 7),
                        Limit = 99,
                        CancelDate = new DateTime(2023, 07, 7)
                }
            }).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(id)).ReturnsAsync(entityPartner);

            var entitySetPartnerPromoCodeLimitRequest = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().With(s => s.Limit, limit).Create();

            //Act
            var actionResult = await _partnersController.SetPartnerPromoCodeLimitAsync(id, entitySetPartnerPromoCodeLimitRequest);

            //Assert
            actionResult.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_new_Limit_save_in_database()
        {
            //Arrange
            Guid id = Guid.NewGuid();

            var entityPartner = new Fixture().Build<Partner>().With(p => p.IsActive, true).With(p => p.NumberIssuedPromoCodes, 10).With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2023, 03, 3),
                        EndDate = new DateTime(2023, 07, 7),
                        Limit = 99,
                        CancelDate = new DateTime(2023, 07, 7)
                }
            }).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(id)).ReturnsAsync(entityPartner);

            var entitySetPartnerPromoCodeLimitRequest = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().With(s => s.Limit, 10).Create();

            //Act
            var actionResult = await _partnersController.SetPartnerPromoCodeLimitAsync(id, entitySetPartnerPromoCodeLimitRequest);

            //Assert
            entityPartner.PartnerLimits.Count.Should().Be(2);
        }
    }
}